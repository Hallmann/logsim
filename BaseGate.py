import inspect
from abc import ABC, abstractmethod


def to_binary(value):
    if type(value) == bool:
        return value
    if type(value) == int:
        if value in [0, 1]:
            return bool(value)
        raise ValueError('only 0 or 1 allowed')
    raise TypeError('only bool or int allowed')


class Pin:
    def __init__(self, readonly=False):
        self.readonly = readonly
        self.value = False

    def __setattr__(self, key, value):
        if key == 'readonly':
            if not hasattr(self, 'readonly'):
                self.__dict__['readonly'] = to_binary(value)
            else:
                raise AttributeError('not allowed to override "readonly"')
        elif key == 'value':
            if hasattr(self, 'value') and self.readonly:
                raise TypeError('this pin is an output and should not be assigned')
            else:
                self.__dict__['value'] = to_binary(value)
        else:
            raise AttributeError('no such attribute')

    def override_readonly_value(self, value):
        self.__dict__['value'] = to_binary(value)


class Port:
    def __init__(self, readonly=False, bits=8):
        self.pins = [Pin(readonly=readonly) for _ in range(bits)]
        self.__dict__['finalized'] = True

    def __setattr__(self, key, value):
        if self.__dict__.get('finalized'):
            self.pins[key] = value
        else:
            super().__setattr__(key, value)

    def __iter__(self):
        return [p.value for p in self.pins].__iter__()

    def values(self):
        return [pin.value for pin in self.pins]


class BaseGate(ABC):
    def __init__(self):
        self.validate()
        self.__dict__['finalized'] = True
        self.execute()

    def __setattr__(self, key, value):
        print('set {}={} for {}'.format(key, value, type(self).__name__))
        stack = inspect.stack()
        the_class = stack[1][0].f_locals["self"]
        print(the_class)
        if self.__dict__.get('finalized'):
            attr = self.__dict__.get(key)
            if isinstance(attr, (Pin, Port)):
                if attr.readonly and not issubclass(type(self), BaseGate):
                    raise ValueError('{} {} is readonly'.format(type(attr).__name__, key))
                print(type(self))
                attr.value = value
            else:
                raise Exception
        else:
            super().__setattr__(key, value)

    @abstractmethod
    def execute(self):
        raise NotImplementedError

    def validate(self):
        for key, value in self.__dict__.items():
            if not type(value) in [Pin, Port]:
                raise Exception('{} is not a pin or a port'.format(key))
