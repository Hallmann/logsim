from SimpleGate import SimpleGate


class AndGate(SimpleGate):
    def execute(self):
        self.Output.override_readonly_value(all(self.Inputs))


a = AndGate()
