from SimpleGate import SimpleGate


class OrGate(SimpleGate):
    def execute(self):
        # noinspection PyProtectedMember
        self.OutputPin._value = any(self.InputBus.get_values())
