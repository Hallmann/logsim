from SimpleGate import SimpleGate


class XorGate(SimpleGate):
    def execute(self):
        # noinspection PyProtectedMember
        self.OutputPin._value = self.InputBus[0].value + self.InputBus[1].value == 1
