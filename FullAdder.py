from BaseGate import BaseGate, Pin, Bus
from HalfAdder import HalfAdder
from OrGate import OrGate


class FullAdder(BaseGate):
    def __init__(self):
        self.halfAdderA = HalfAdder()
        self.halfAdderB = HalfAdder()
        self.orGate = OrGate()

        self.CarryInPin = Pin('carry in', readonly=False)
        self.InputBus = Bus('summands', readonly=False, bits=2)
        self.SumPin = Pin('sum', readonly=True)
        self.CarryOutPin = Pin('carry out', readonly=True)

        super().__init__()

    def execute(self):
        self.halfAdderA.InputBus[0] = self.InputBus[0].value
        self.halfAdderA.InputBus[1] = self.InputBus[1].value

        self.halfAdderA.execute()

        self.halfAdderB.InputBus[0] = self.halfAdderA.SumPin.value
        self.halfAdderB.InputBus[1] = self.CarryInPin.value

        self.halfAdderB.execute()

        self.orGate.InputBus[0] = self.halfAdderB.CarryOutPin.value
        self.orGate.InputBus[1] = self.halfAdderA.CarryOutPin.value

        self.orGate.execute()

        # Output is an inner class and warns about accessing _pins
        # noinspection PyProtectedMember
        self.SumPin._value = self.halfAdderB.SumPin.value
        # noinspection PyProtectedMember
        self.CarryOutPin._value = self.orGate.OutputPin.value
