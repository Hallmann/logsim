from AndGate import AndGate
from BaseGate import BaseGate, Pin, Bus
from XorGate import XorGate


class HalfAdder(BaseGate):
    def __init__(self):
        self.andGate = AndGate()
        self.xorGate = XorGate()
        self.CarryOutPin = Pin('carry out', readonly=True)
        self.SumPin = Pin('sum', readonly=True)
        self.InputBus = Bus('inputs', readonly=False, bits=2)
        super().__init__()

    def execute(self):
        self.andGate.InputBus[0] = self.InputBus[0].value
        self.andGate.InputBus[1] = self.InputBus[1].value
        self.xorGate.InputBus[0] = self.InputBus[0].value
        self.xorGate.InputBus[1] = self.InputBus[1].value

        self.andGate.execute()
        self.xorGate.execute()

        # Output is an inner class and warns about accessing _pins
        # noinspection PyProtectedMember
        self.SumPin._value = self.xorGate.OutputPin.value
        # noinspection PyProtectedMember
        self.CarryOutPin._value = self.andGate.OutputPin.value
