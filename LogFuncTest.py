import unittest
from itertools import product

from AndGate import AndGate
from BaseGate import BaseGate
from CarryRippleAdder import CarryRippleAdder
from FullAdder import FullAdder
from HalfAdder import HalfAdder
from NandGate import NandGate
from OrGate import OrGate
from XorGate import XorGate


class BaseGateTest(unittest.TestCase):
    def test_case_01(self):
        self.assertRaises(TypeError, BaseGate)


class AndGateTest(unittest.TestCase):
    def test_case_01(self):
        g = AndGate()
        g.InputBus[0] = False
        g.InputBus[1] = False
        g.execute()
        self.assertFalse(g.OutputPin.value)

    def test_case_02(self):
        g = AndGate()
        g.InputBus[0] = False
        g.InputBus[1] = True
        g.execute()
        self.assertFalse(g.OutputPin.value)

    def test_case_03(self):
        g = AndGate()
        g.InputBus[0] = True
        g.InputBus[1] = False
        g.execute()
        self.assertFalse(g.OutputPin.value)

    def test_case_04(self):
        g = AndGate()
        g.InputBus[0] = True
        g.InputBus[1] = True
        g.execute()
        self.assertTrue(g.OutputPin.value)


class NandGateTest(unittest.TestCase):
    def test_case_01(self):
        g = NandGate()
        g.InputBus[0] = False
        g.InputBus[1] = False
        g.execute()
        self.assertTrue(g.OutputPin.value)

    def test_case_02(self):
        g = NandGate()
        g.InputBus[0] = False
        g.InputBus[1] = True
        g.execute()
        self.assertTrue(g.OutputPin.value)

    def test_case_03(self):
        g = NandGate()
        g.InputBus[0] = True
        g.InputBus[1] = False
        g.execute()
        self.assertTrue(g.OutputPin.value)

    def test_case_04(self):
        g = NandGate()
        g.InputBus[0] = True
        g.InputBus[1] = True
        g.execute()
        self.assertFalse(g.OutputPin.value)


class OrGateTest(unittest.TestCase):
    def test_case_01(self):
        g = OrGate()
        g.InputBus[0] = False
        g.InputBus[1] = False
        g.execute()
        self.assertFalse(g.OutputPin.value)

    def test_case_02(self):
        g = OrGate()
        g.InputBus[0] = False
        g.InputBus[1] = True
        g.execute()
        self.assertTrue(g.OutputPin.value)

    def test_case_03(self):
        g = OrGate()
        g.InputBus[0] = True
        g.InputBus[1] = False
        g.execute()
        self.assertTrue(g.OutputPin.value)

    def test_case_04(self):
        g = OrGate()
        g.InputBus[0] = True
        g.InputBus[1] = True
        g.execute()
        self.assertTrue(g.OutputPin.value)


class XorGateTest(unittest.TestCase):
    def test_case_01(self):
        g = XorGate()
        g.InputBus[0] = False
        g.InputBus[1] = False
        g.execute()
        self.assertFalse(g.OutputPin.value)

    def test_case_02(self):
        g = XorGate()
        g.InputBus[0] = False
        g.InputBus[1] = True
        g.execute()
        self.assertTrue(g.OutputPin.value)

    def test_case_03(self):
        g = XorGate()
        g.InputBus[0] = True
        g.InputBus[1] = False
        g.execute()
        self.assertTrue(g.OutputPin.value)

    def test_case_04(self):
        g = XorGate()
        g.InputBus[0] = True
        g.InputBus[1] = True
        g.execute()
        self.assertFalse(g.OutputPin.value)


class HalfAdderTest(unittest.TestCase):
    def test_case_01(self):
        g = HalfAdder()
        g.InputBus[0] = False
        g.InputBus[1] = False
        g.execute()
        self.assertFalse(g.SumPin.value)
        self.assertFalse(g.CarryOutPin.value)

    def test_case_02(self):
        g = HalfAdder()
        g.InputBus[0] = False
        g.InputBus[1] = True
        g.execute()
        self.assertTrue(g.SumPin.value)
        self.assertFalse(g.CarryOutPin.value)

    def test_case_03(self):
        g = HalfAdder()
        g.InputBus[0] = True
        g.InputBus[1] = False
        g.execute()
        self.assertTrue(g.SumPin.value)
        self.assertFalse(g.CarryOutPin.value)

    def test_case_04(self):
        g = HalfAdder()
        g.InputBus[0] = True
        g.InputBus[1] = True
        g.execute()
        self.assertFalse(g.SumPin.value)
        self.assertTrue(g.CarryOutPin.value)


class FullAdderTest(unittest.TestCase):
    def test_case_01(self):
        # product returns all cartesian products of the first parameter
        # in this case, all possible boolean combinations of a, b and carryIn are tried once
        for a, b, carryIn in product([False, True], repeat=3):
            g = FullAdder()
            g.InputBus[0] = a
            g.InputBus[1] = b
            g.CarryInPin.value = carryIn

            g.execute()

            # you can do math with boolean values. true = 1 and false = 0
            # carry bit represents 2^1 = 2, sum bit represents 2^0 = 1
            # for example: a = 1, b = 0, c = 1 results in carry = 1, sum = 0, which can be interpreted as 2 in decimal
            simulatedSum = 2 * g.CarryOutPin.value + 1 * g.SumPin.value
            # adding all positive inputs should result in the same sum
            expectedSum = sum([a, b, carryIn])
            self.assertEqual(simulatedSum, expectedSum)


class CarryRippleAdderTest(unittest.TestCase):
    def test_case_01(self):
        bits = 4
        g = CarryRippleAdder(bits=bits)
        g.SummandA.set_values(1, 1, 1, 1)

        g.SummandB.set_values(0, 0, 1, 0)

        g.execute()

        print(g.SumBus)
        self.assertEqual(g.SumBus.get_values(), [True, True, False, False])


if __name__ == '__main__':
    unittest.main()
