from BaseGate import BaseGate, Bus, Pin
from FullAdder import FullAdder


class CarryRippleAdder(BaseGate):
    def __init__(self, bits=8):
        self.SummandA = Bus('summand A', readonly=False, bits=bits)
        self.SummandB = Bus('summand B', readonly=False, bits=bits)
        self.SumBus = Bus('sum', readonly=True, bits=bits)
        self.CarryIn = Pin('carry in', readonly=False)
        self.CarryOut = Pin('carry out', readonly=True)

        self.fullAdders = [FullAdder() for _ in range(bits)]

        super().__init__()

    def execute(self):
        for i in range(len(self.fullAdders)):
            if i == 0:
                self.fullAdders[i].CarryInPin.value = self.CarryIn.value
            else:
                self.fullAdders[i].CarryInPin.value = self.fullAdders[i - 1].CarryOutPin.value
            self.fullAdders[i].InputBus[0] = self.SummandA[i].value
            self.fullAdders[i].InputBus[1] = self.SummandB[i].value
            self.fullAdders[i].execute()
            self.SumBus[i]._value = self.fullAdders[i].SumPin.value
        self.CarryOut._value = self.fullAdders[-1].CarryOutPin.value
