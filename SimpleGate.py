from BaseGate import BaseGate, Pin, Port


# noinspection PyAbstractClass
class SimpleGate(BaseGate):

    def __init__(self, num_inputs=2):
        if not type(num_inputs) == int:
            raise TypeError('expected int >= {}'.format(self.min_inputs))
        if num_inputs < self.min_inputs:
            raise ValueError('need at least {} inputs'.format(self.min_inputs))
        if num_inputs == 1:
            self.Input = Pin(readonly=False)
        else:
            self.Inputs = Port(readonly=False, bits=num_inputs)
        self.Output = Pin(readonly=True)
        super().__init__()

    @property
    def min_inputs(self):
        return 2
