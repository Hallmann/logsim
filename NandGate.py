from SimpleGate import SimpleGate


class NandGate(SimpleGate):
    def execute(self):
        # noinspection PyProtectedMember
        self.OutputPin._value = not all(self.InputBus.get_values())
