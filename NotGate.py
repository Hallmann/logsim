from BaseGate import BaseGate, Pin


class NotGate(BaseGate):
    def __init__(self):
        self.Input = Pin('input', readonly=False)
        self.Output = Pin('output', readonly=True)
        super().__init__()

    def execute(self):
        self.Output._value = not self.Input.value
