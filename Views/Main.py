"""import tkinter as tk
from abc import ABC
from itertools import count

from AndGate import AndGate
from BaseGate import BaseGate


class ViewGate:
    def __init__(self, canvas: tk.Canvas, gate: BaseGate, x, y):
        self.canvas = canvas
        canvas.create_rectangle(x, y, x + 50, y + 100, fill='#fff', width=4, activewidth=6)
        canvas.create_line(x - 10, y + 25, x, y + 25, width=4)
        self.text_id = canvas.create_text(x + 25, y + 50)

    def set_text(self, text):
        self.canvas.configure(self.text_id, text=text)


class GateRegistry:
    def __init__(self):
        self.gates = dict()

    def register(self, gate: BaseGate, view_gate: ViewGate):
        for id in count():
            if not id in self.gates:
                self.gates[id] = (gate, view_gate)
                view_gate.set_text(id)


class MouseController:
    def __init__(self, canvas: tk.Canvas):
        self.selected = []
        self.dnd_components = []
        self.dnd_last_pos = None
        self.canvas = canvas
        self.canvas.bind('<Button-1>', self.left_mouse_down)
        self.canvas.bind('<B1-Motion>', self.left_mouse_move)
        self.canvas.bind('<ButtonRelease-1>', self.left_mouse_up)

    def register_for_drag_and_drop(self, component):
        self.dnd_components.append(component)

    def left_mouse_down(self, event):
        below_mouse = [component for component in self.dnd_components if component.is_below_mouse(event.x, event.y)]
        if below_mouse:
            for component in below_mouse:
                self.selected.append(component)
                component.configure(fill=component.color_selected)

            self.dnd_last_pos = (event.x, event.y)
        else:
            self.deselect_all()

    def left_mouse_move(self, event):
        print(event)
        for component in self.selected:
            component.move_relative(event.x - self.dnd_last_pos[0], event.y - self.dnd_last_pos[1])

        self.dnd_last_pos = (event.x, event.y)
        self.canvas.configure(background='white')  # redraw canvas because vBox sux

    def left_mouse_up(self, event):
        self.deselect_all()

    def deselect_all(self):
        for component in self.selected:
            component.configure(fill=component.color)
        self.selected = []


color_background = '#aaa'


class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.pack()
        self.canvas = SimulatorCanvas(self, 800, 600)

        g = AndGate()
        vg = ViewGate(self.canvas, g, 100, 100)


class SimulatorCanvas(tk.Canvas):
    def __init__(self, master, width, height):
        super().__init__(master, width=width, height=height, background=color_background)
        self.pack()
        self.bind('<Button-1>', self.left_mouse_down)
        self.bind('<Button-3>', self.right_mouse_down)
        self.gates = []

    def left_mouse_down(self, event):
        component = self.find_overlapping(event.x, event.y, event.x, event.y)
        if component:
            print(component)

    def right_mouse_down(self, event):
        g = AndGate()
        vg = ViewGate(self, g, event.x, event.y)


root = tk.Tk()
app = Application(master=root)
app.mainloop()
"""
